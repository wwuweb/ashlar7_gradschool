# Western Washington University Drupal 7 Theme - Ashlar #
A Drupal Zen Sub-Theme.

##Dependencies##
  * [Zen 5.x](http://drupal.org/project/zen)
  * [Ultimenu](https://www.drupal.org/project/ultimenu)
  * Possibly some other things

This is an extension of the [Ashlar](https://bitbucket.org/wwuweb/ashlar/) Drupal 8 theme, made for Drupal 7. It is a work in progress.

* This theme does not have pattern lab installed, nor the folder structure. Instead, it has a copy of the components.css output by the main Ashlar theme as a sass file. It also has a few pieces copied from that theme, including sass and js files.

* The layouts from the D8 version of Ashlar have been translated as panel layouts.
