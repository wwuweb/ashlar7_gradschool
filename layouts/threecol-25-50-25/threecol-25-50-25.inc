<?php

// Plugin definition
$plugin = array(
  'title' => t('Three Column 25/50/25'),
  'category' => t('Ashlar'),
  'icon' => 'threecol-25-50-25.png',
  'theme' => 'threecol-25-50-25',
  'css' => '../../css/panel-layouts/threecol-25-50-25.css',
  'regions' => array(
    'top' => t('Top'),
    'first' => t('Left'),
    'second' => t('Middle'),
    'third' => t('Right'),
    'bottom' => t('Bottom')
  ),
);
