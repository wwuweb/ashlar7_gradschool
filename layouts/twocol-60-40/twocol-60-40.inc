<?php

// Plugin definition
$plugin = array(
  'title' => t('Two Column 60/40'),
  'category' => t('Ashlar'),
  'icon' => 'twocol-60-40.png',
  'theme' => 'twocol-60-40',
  'css' => '../../css/panel-layouts/twocol-60-40.css',
  'regions' => array(
    'top' => t('Top'),
    'first' => t('Left'),
    'second' => t('Right'),
    'bottom' => t('Bottom')
  ),
);
