<?php

// Plugin definition
$plugin = array(
  'title' => t('Two Column 75/25'),
  'category' => t('Ashlar'),
  'icon' => 'twocol-75-25.png',
  'theme' => 'twocol-75-25',
  'css' => '../../css/panel-layouts/twocol-75-25.css',
  'regions' => array(
    'top' => t('Top'),
    'first' => t('Left'),
    'second' => t('Right'),
    'bottom' => t('Bottom')
  ),
);
