<?php
/**
 * @file
 * Template for a layout with two columns
 *
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['top']: Content in the top region
 *   - $content['first']: Content in the left column
 *   - $content['second']: Content in the right column
 *   - $content['bottom']: Content in the bottom region
 */
?>
<div class="layout layout--twocol-bricks" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
	<div class="layout__region layout__region--top" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
		<?php print $content['top']; ?>
	</div>

	<div class="layout__region layout__region--first-above" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
		<?php print $content['first_above']; ?>
	</div>

	<div class="layout__region layout__region--second-above" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
		<?php print $content['second_above']; ?>
	</div>

	<div class="layout__region layout__region--middle" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
		<?php print $content['middle']; ?>
	</div>

	<div class="layout__region layout__region--first-below" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
		<?php print $content['first_below']; ?>
	</div>

	<div class="layout__region layout__region--second-below" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
		<?php print $content['second_below']; ?>
	</div>

	<div class="layout__region layout__region--bottom" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
		<?php print $content['bottom']; ?>
	</div>
</div>
