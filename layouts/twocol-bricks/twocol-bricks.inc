<?php

// Plugin definition
$plugin = array(
  'title' => t('Two Column Bricks'),
  'category' => t('Ashlar'),
  'icon' => 'twocol-bricks.png',
  'theme' => 'twocol-bricks',
  'css' => '../../css/panel-layouts/twocol-bricks.css',
  'regions' => array(
    'top' => t('Top'),
    'first_above' => t('Top Left'),
    'second_above' => t('Top Right'),
    'middle' => t('Middle'),
    'first_below' => t('Bottom Left'),
    'second_below' => t('Bottom Right'),
    'bottom' => t('Bottom')
  ),
);
