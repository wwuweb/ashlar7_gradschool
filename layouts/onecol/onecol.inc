<?php

// Plugin definition
$plugin = array(
  'title' => t('One Column'),
  'category' => t('Ashlar'),
  'icon' => 'onecol.png',
  'theme' => 'onecol',
  'css' => '../../css/panel-layouts/onecol.css',
  'regions' => array(
    'first' => t('Content Area')
  ),
);
